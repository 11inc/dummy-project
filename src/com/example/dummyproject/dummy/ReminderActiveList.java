package com.example.dummyproject.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class ReminderActiveList {

	/**
	 * An array of sample (dummy) items.
	 */
	public static List<ReminderItem> ITEMS = new ArrayList<ReminderItem>();

	/**
	 * A map of sample (dummy) items, by ID.
	 */
	public static Map<String, ReminderItem> ITEM_MAP = new HashMap<String, ReminderItem>();

	public static void addItem(ReminderItem item) {
		ITEMS.add(item);
		ITEM_MAP.put(item.id, item);
	}
	
	public static void removeItem(ReminderItem item){
		ITEMS.remove(item);
		ITEM_MAP.remove(item.id);	
	}
}
