package com.example.dummyproject.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReminderDeliveredList {

	/**
	 * An array of sample (dummy) items.
	 */
	public static List<ReminderItem> ITEMS = new ArrayList<ReminderItem>();

	/**
	 * A map of sample (dummy) items, by ID.
	 */
	public static Map<String, ReminderItem> ITEM_MAP = new HashMap<String, ReminderItem>();

	public static void addItem(ReminderItem item) {
		ITEMS.add(item);
		ITEM_MAP.put(item.id, item);
	}
	
	public static void removeItem(ReminderItem item){
		ITEMS.remove(item);
		ITEM_MAP.remove(item.id);	
	}
}
