package com.example.dummyproject.dummy;

import com.google.android.gms.maps.model.LatLng;


public class ReminderItem {
	public String id;
	public String title;
	public String description;
	public LatLng position;

	public ReminderItem(String id, String title, String description,
			LatLng position) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.position = position;
	}

	@Override
	public String toString() {
		return title;
	}
}